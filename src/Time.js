import React from 'react';
import "./Time.css";

export class Clock extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            date: new Date()

        };
    }

    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        this.setState({
            date: new Date()
        });
    }

    render() {
        return (
            <div className={"displayTime"}>
                <h1>{this.state.date.toLocaleTimeString([], {timeStyle: 'short'})}</h1>
            </div>
        );
    }
}
