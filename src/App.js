import React from 'react';
import './App.css';
import logo from "./logo.svg";
import {Clock} from './Time';
import {Schedule} from "./Schedule.js"

function App() {
  return (
    <div className="App">
        <div>
            <div className={"logos"}>
                <img src={logo} alt={""} width={100} className={'SULogo'}/>
            </div>
            <Clock />
            <Schedule />
            <h1>Welcome!</h1>
        </div>
    </div>

  );
}

export default App;
