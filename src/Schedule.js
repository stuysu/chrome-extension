import React from "react"

function getTimesFromStrings(arr) {
	return arr.map(time => {
		const [_, hour, minute, period] = /(\d+?):(\d+?) ((?:A|P)M)/ig.exec(time)
		return Number(minute) + ((Number(hour) % 12) * 60) + ({"AM":0, "PM":12}[period] * 60)
	})
}

function minuteOfDay() {
	const date = new Date()
	return (date.getHours() * 60) + date.getMinutes()
}

export class Schedule extends React.Component {
	constructor(props) {
		super(props)
		//timeSet tracks when the schedule is chosen.
		//in tick, if timeSet is not the same day as now, it flushes that choice.
		this.state = {schedule: "", timeSet: new Date()}
		this.schedules = {
			"Regular": [
				["Period 1", ["8:00 AM", "8:41 AM"]],
				["Period 2", ["8:45 AM", "9:26 AM"]],
				["Period 3", ["9:31 AM", "10:15 AM"]],
				["Period 4", ["10:20 AM", "11:01 AM"]],
				["Period 5", ["11:06 AM", "11:47 AM"]],
				["Period 6", ["11:52 AM", "12:33 PM"]],
				["Period 7", ["12:38 PM", "1:19 PM"]],
				["Period 8", ["1:24 PM", "2:05 PM"]],
				["Period 9", ["2:09 PM", "2:50 PM"]],
				["Period 10", ["2:54 PM", "3:35 PM"]]
			],
			"Homeroom": [
				["Period 1", ["8:00 AM", "8:40 AM"]],
				["Period 2", ["8:45 AM", "9:25 AM"]],
				["Period 3", ["9:29 AM", "10:09 AM"]],
				["Homeroom", ["10:13 AM", "10:25 AM"]],
				["Period 4", ["10:30 AM", "11:10 AM"]],
				["Period 5", ["11:14 AM", "11:54 AM"]],
				["Period 6", ["11:58 AM", "12:38 PM"]],
				["Period 7", ["12:42 PM", "1:22 PM"]],
				["Period 8", ["1:26 PM", "2:06 PM"]],
				["Period 9", ["2:10 PM", "2:50 PM"]],
				["Period 10", ["2:55 PM", "3:35 PM"]]
			],
			"Conferences": [
				["Period 1", ["8:00 AM", "8:37 AM"]],
				["Period 2", ["8:41 AM", "9:18 AM"]],
				["Period 3", ["9:22 AM", "9:59 AM"]],
				["Period 4", ["10:03 AM", "10:40 AM"]],
				["Period 5", ["10:44 AM", "11:21 AM"]],
				["Period 6", ["11:25 AM", "12:02 PM"]],
				["Period 7", ["12:06 PM", "12:43 PM"]],
				["Period 8", ["12:47 PM", "1:24 PM"]],
				["Period 9", ["1:28 PM", "2:05 PM"]],
				["Period 10", ["2:09 PM", "2:46 PM"]],
				["Meeting", ["2:50 PM", "3:30 PM"]]
			]
		}
	}
	componentDidMount() {
		let oldSchedule = localStorage.getItem("schedule")
		let timeSet = new Date()
		if (oldSchedule !== "") {
			const now = new Date()
			const then = new Date(Number(localStorage.getItem("timeSet")))
			if (now.getDate() !== then.getDate() || now.getMonth() !== then.getMonth()) {
				oldSchedule = ""
				localStorage.removeItem("schedule")
				localStorage.removeItem("timeSet")
			} else {
				timeSet = then
			}
		}
		this.setState({schedule: oldSchedule, timeSet: timeSet})
		this.timerID = setInterval(() => this.tick(), 60000)
	}
	componentWillUnmount() {
		clearInterval(this.timerID)
	}
	tick() {
		const now = new Date()
		if (this.state.timeSet.getDate() !== now.getDate() || this.state.timeSet.getMonth() !== now.getMonth()) {
			this.setState({schedule: "", timeSet: new Date()})
		} else {
			this.setState(this.state)
		}
	}
	renderSchedule(scheduleName) {
		let currPeriod = this.schedules[scheduleName][0][0]
		let timeLeft = "There are -1 minutes left." //string for reasons
		let minute = minuteOfDay()
		if (minute < 480) { //480 = 8:00
			currPeriod = "Before School"
			timeLeft = 480 - minuteOfDay()
		}
		const schedule = [
			(
				<tr>
					<th>Period</th>
					<th>Time</th>
				</tr>
			)
		]
		this.schedules[scheduleName].forEach((period, periodNumber) => {
			const periodTime = getTimesFromStrings(period[1])
			if (minute >= periodTime[1]) {
				if (periodNumber === this.schedules[scheduleName].length - 1) {
					currPeriod = "After School"
					timeLeft = "" //this is reason
				} else {
					const nextPeriod = this.schedules[scheduleName][periodNumber + 1]
					if (minute > getTimesFromStrings(nextPeriod[1])[0]) {
						currPeriod = `Before ${nextPeriod[0]}`
						const minLeft = getTimesFromStrings(nextPeriod[1])[0] - minute
						timeLeft = minLeft === 1 ? "There is 1 minute left." : `There are ${minLeft} minutes left.`
					}
				}
			} else if (minute <= periodTime[1] && minute >= periodTime[0]) {
				currPeriod = period[0]
				const minLeft = periodTime[1] - minute
				timeLeft = minLeft === 1 ? "There is 1 minute left." : `There are ${minLeft} minutes left.`
			}
			schedule.push(
				<tr>
					<th>{period[0]}</th>
					<th>{period[1].join(" to ")}</th>
				</tr>
			)
		})
		let classes = ""
		if (this.state.schedule === scheduleName) classes += "active"
		return (
			<div class={classes}>
				<h2>{scheduleName}</h2>
				<table>
					{schedule}
				</table>
				<span id="period">It is currently {currPeriod}. {timeLeft}</span><br/>
				<button class="use" onClick={() => this.chooseSchedule(scheduleName)}>Use</button>
				<button class="change" onClick={() => this.changeSchedule()}>Change</button>
			</div>
		)
	}
	chooseSchedule(scheduleName) {
		this.setState({schedule: scheduleName, timeSet: new Date()})
		localStorage.setItem("schedule", scheduleName)
		localStorage.setItem("timeSet", Date.now())
	}
	changeSchedule() {
		localStorage.removeItem("timeSet")
		localStorage.removeItem("schedule")
		this.setState({schedule: "", timeSet: new Date()})
	}
	render() {
		const schedules = []
		if (this.state.schedule === "") {
			Object.keys(this.schedules).forEach(scheduleName => {
				schedules.push(this.renderSchedule(scheduleName))
			})
		} else {
			schedules.push(this.renderSchedule(this.state.schedule))
		}
		return (
			<>
				<h1>Schedules</h1>
				<div id="scheduleContainer">
					{schedules}
				</div>
			</>
		)
	}
}


